import telebot
import Config

bot = telebot.TeleBot(Config.token)


@bot.message_handler(commands=['start'])
def handle_command(com):
    bot.reply_to(com, "Это тестовый бот")


@bot.message_handler(content_types=['new_chat_members'])
def get_text(message):
    count = bot.get_chat_members_count(message.chat.id)
    if count == 999:
        bot.send_message(message.chat.id, "Еще один участник и нас будет 1000!!! ")
    elif count == 1000:
        bot.send_message(message.chat.id, "Воу Воу Воу! Нас уже 1000 человек! Спасибо всем участникам нашего чата!")
    for user in message.new_chat_members:
        bot.send_message(message.chat.id, "&#128075 Привет, <a href=\"tg://user?id={0}\"> {1}! </a> Рады видеть тебя в нашем сообществе!\n\n"
                                        "Это чат YouTube канала "
                                         "<a href=\"https://www.youtube.com/channel/UChcuP_3gqyNiYZeOFDNK-aw\"> IT на Dиване</a>.\n"
                                         "Ознакомься пожалуйста с <a href=\"https://t.me/it_na_divane/3853\"> Правилами </a>"
                                          "и не нарушай их.\nТак же мы собираем <a href=\"https://telegra.ph/FAQ---IT-na-Divane-01-14\"> Страничку </a> с "
                                          "полезными ресурсами для обучения."
                        .format(user.id, user.first_name), parse_mode='html', disable_web_page_preview=1)

bot.polling()
